# -*- coding: utf-8 -*-

from django.contrib import admin
from models import Teacher, CourseName, Course, Schedule,\
    DaySchedule, Event, Calandar, DayCalandar, DayName


class CourseNameAdmin(admin.ModelAdmin):
    pass
admin.site.register(CourseName, CourseNameAdmin)


class TeacherAdmin(admin.ModelAdmin):
    pass
admin.site.register(Teacher, TeacherAdmin)

'''
class CourseCategoryAdmin(admin.ModelAdmin):
    pass
admin.site.register(CourseCategory, CourseCategoryAdmin)
'''

class CourseAdmin(admin.ModelAdmin):
    pass
admin.site.register(Course, CourseAdmin)


class ScheduleAdmin(admin.ModelAdmin):
    def day_schedules(self, inst):
        return ','.join([str(b) for b in inst.dayschedule_set.all()])

    list_display = ('name', 'day_schedules')

admin.site.register(Schedule, ScheduleAdmin)


class DayScheduleAdmin(admin.ModelAdmin):
    pass
admin.site.register(DaySchedule, DayScheduleAdmin)


class DayNameAdmin(admin.ModelAdmin):
    pass
admin.site.register(DayName, DayNameAdmin)


class EventAdmin(admin.ModelAdmin):
    pass
admin.site.register(Event, EventAdmin)


class CalandarAdmin(admin.ModelAdmin):
    pass
admin.site.register(Calandar, CalandarAdmin)


class DayCalandarAdmin(admin.ModelAdmin):
    pass
admin.site.register(DayCalandar, DayCalandarAdmin)