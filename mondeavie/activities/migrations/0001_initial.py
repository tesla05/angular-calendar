# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Calandar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('course_type', models.CharField(max_length=50, blank=True)),
                ('note', models.CharField(max_length=512, blank=True)),
                ('image', models.ImageField(null=True, upload_to=b'course_pic', blank=True)),
                ('body_txt', models.TextField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CourseName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DayCalandar',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.DateTimeField()),
                ('start_hour', models.TimeField()),
                ('end_hour', models.TimeField()),
                ('calandar', models.ForeignKey(to='activities.Calandar')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DayName',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DaySchedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hour_start', models.TimeField()),
                ('hour_end', models.TimeField()),
                ('day_name', models.ForeignKey(to='activities.DayName')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('day_start', models.DateField()),
                ('day_end', models.DateField()),
                ('course', models.ForeignKey(to='activities.Course')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('tel', models.CharField(max_length=15, blank=True)),
                ('school_name', models.CharField(max_length=30, blank=True)),
                ('school_url', models.URLField(max_length=100, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='event',
            name='teachers',
            field=models.ManyToManyField(to='activities.Teacher'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='dayschedule',
            name='schedule',
            field=models.ForeignKey(to='activities.Schedule'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='course',
            name='course_name',
            field=models.ForeignKey(to='activities.CourseName', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='course',
            name='teacher',
            field=models.ForeignKey(to='activities.Teacher'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='course',
            unique_together=set([('teacher', 'course_name')]),
        ),
        migrations.AddField(
            model_name='calandar',
            name='event',
            field=models.ForeignKey(to='activities.Event', unique=True),
            preserve_default=True,
        ),
    ]
