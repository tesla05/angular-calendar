# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Teacher(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    tel = models.CharField(max_length=15, blank=True)
    school_name = models.CharField(max_length=30, blank=True)
    school_url = models.URLField(max_length=100, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.first_name, self.last_name)

'''
Course section
'''
class CourseName(models.Model):
    # Yoga
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return u'%s' % (self.name)

class Course(models.Model):
    teacher = models.ForeignKey(Teacher)
    # Yoga
    course_name = models.ForeignKey(CourseName, blank=True)

    #Hatha et Vinyasa Yoga 
    course_type = models.CharField(max_length=50, blank=True)
    # Journees Feriees seront reporte...
    note = models.CharField(max_length=512, blank=True)
    image = models.ImageField(upload_to='course_pic', null=True, blank=True)
    body_txt = models.TextField(blank=True)

    def __unicode__(self):
        return u'%s avec %s' % (self.course_name, self.teacher)

    class Meta:
        unique_together = (("teacher", "course_name"),)

class Schedule(models.Model):
    # many Courses
    course = models.ForeignKey(Course)

    # Yoga Doux
    name = models.CharField(max_length=30)
    # 15 Jan au 5 mars
    day_start = models.DateField()
    day_end = models.DateField()

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        pass
    	#unique_together = (("course_category", "course"),)
        #verbose_name = _('schedule')
        #verbose_name_plural = _('schedules')

'''
    LUNDI = 0
    MARDI = 1
    MERCREDI = 2
    JEUDI = 3
    VENDREDI = 4
    SAMEDI = 5
    DIMANCHE = 6

    day_name_choises = ((LUNDI, u'Lundi'),
                        (MARDI, u'Mardi'),
                        (MERCREDI, u'Mercredi'),
                        (JEUDI, u'Jeudi'),
                        (VENDREDI, u'Vendredi'),
                        (SAMEDI, u'Samedi'),
                        (DIMANCHE, u'Dimanche'))
'''

class DayName(models.Model):
    name = models.CharField(max_length=10)
    def __unicode__(self):
        return u'%s' % (self.name)

class DaySchedule(models.Model):
    schedule = models.ForeignKey(Schedule)
    day_name = models.ForeignKey(DayName)
    hour_start = models.TimeField()
    hour_end = models.TimeField()
    

    def __unicode__(self):
        return u'%s: de %s a %s' % (self.day_name, self.hour_start, self.hour_end)


'''
Event section
'''
class Event(models.Model):
    teachers = models.ManyToManyField(Teacher)
    name = models.CharField(max_length=30)


class Calandar(models.Model):
    event = models.ForeignKey(Event, unique=True)
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
    	pass
        #verbose_name = _('schedule')
        #verbose_name_plural = _('schedules')


class DayCalandar(models.Model):
    calandar = models.ForeignKey(Calandar)
    day = models.DateTimeField()
    start_hour = models.TimeField()
    end_hour = models.TimeField()

    def __unicode__(self):
        return u'%s: de %s a %s' % (self.day, self.start_hour, self.end_hour)
