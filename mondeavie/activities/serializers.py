# -*- coding: utf-8 -*-

import base64
from django.forms import widgets
from rest_framework import serializers
from django.core.files.base import ContentFile
from activities.models import Teacher, CourseName, Course, Schedule, DaySchedule,\
        DayName


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ('id', 'first_name', 'last_name', 'tel', 'school_name', 'school_url')


class CourseNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseName
        fields = ('id', 'name',)


class Base64ImageField(serializers.ImageField):

    # Model Object to JSON
    def to_native(self, obj):
        if obj and hasattr(obj, 'url'):
            return obj.url
        return ""

    # https://gist.github.com/yprez/7704036
    # JSON to Model object
    def from_native(self, data):
        if isinstance(data, basestring) and data.startswith('data:image'):
            # base64 encoded image - decode
            format, imgstr = data.split(';base64,') # format ~= data:image/X,
            ext = format.split('/')[-1] # guess file extension

            data = ContentFile(base64.b64decode(imgstr), name='temp.' + ext)
            return super(Base64ImageField, self).from_native(data)
        return data

class CourseSerializer(serializers.ModelSerializer):
    obj_unicode = serializers.SerializerMethodField('obj_unicode_fct')
    image = Base64ImageField(blank=True) # Field optional

    def obj_unicode_fct(self, obj):
        return obj 

    class Meta:
        model = Course
        fields = ('id', 'teacher', 'course_name', 'obj_unicode', 'course_type',\
            'note', 'image', 'body_txt')


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('id', 'course', 'name', 'day_start', 'day_end',)


class DayNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('id', 'name',)


class DayScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = DaySchedule
        fields = ('id', 'schedule', 'day_name', 'day_name', 'hour_start', 'hour_end',)