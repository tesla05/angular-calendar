
/* global angular */
var appActivities = angular.module('appActivities', ['ngRoute', 'ngCookies',
            'ui.bootstrap','dialogs.services']);

/* Config */
appActivities.config(function($interpolateProvider) {
    //allow django templates and angular to co-exist
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});

appActivities.run(function($rootScope, $log, $http, $cookies, $templateCache) {
    $http.defaults.headers.common['X-CSRFToken'] = $cookies['csrftoken'];
});


appActivities.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }
]);

appActivities.directive("ngFileSelect",function(){
    return {
        link: function($scope, el){
            el.bind("change", function(e){          
                $scope.file = (e.srcElement || e.target).files[0];
                $scope.getFile();
            });
        }
    };
});

appActivities.directive("errormsg",function(){
    return {
        scope:{
            saveError:"@"
        },
        template: '<div style="color:red">{[{ saveError }]}</div>',
        link: function($scope, element, attrs){
            if(attrs.saveError){
                alert('error');
            }
        }
    };
});

appActivities.service('dataService', function($http) {
    this.get = function(url, callbackFunc) {
        $http({
            method: 'GET',
            url: url
         }).success(function(data){
            // With the data succesfully returned, call our callback
            callbackFunc(data);
        }).error(function(){
            alert("error");
        });
    };

    this.create = function(url, data, callbackFunc) {
        $http({
            method: 'POST',
            data: data,
            url: url
         }).success(function(data){
            // With the data succesfully returned, call our callback
            callbackFunc(data);
        }).error(function(){
            alert("error");
        });
    };

});

appActivities.service('refreshService', function($http) {

});

/*************************************************************************
 * The controller
 ************************************************************************/


appActivities.run(function($templateCache) {
    //$templateCache.put('/dialogs/notify.html','<div class="modal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title"><span class="glyphicon glyphicon-star"></span> User\'s Name</h4></div><div class="modal-body"><ng-form name="nameDialog" novalidate role="form"><div class="form-group input-group-lg" ng-class="{true: \'has-error\'}[nameDialog.username.$dirty && nameDialog.username.$invalid]"><label class="control-label" for="username">Name:</label><input type="text" class="form-control" name="username" id="username" ng-model="user.name" ng-keyup="hitEnter($event)" required><span class="help-block">Enter your full name, first &amp; last.</span></div></ng-form></div><div class="modal-footer"><button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button><button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine">Save</button></div></div></div></div>');
});

    
appActivities.factory("fileReader", ["$q", "$log", function($q, $log) {

    var onLoad = function(reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.resolve(reader.result);
            });
        };
    };

    var onError = function (reader, deferred, scope) {
        return function () {
            scope.$apply(function () {
                deferred.reject(reader.result);
            });
        };
    };

    var onProgress = function(reader, scope) {
        return function (event) {
            scope.$broadcast("fileProgress",
                {
                    total: event.total,
                    loaded: event.loaded
                });
        };
    };

    var getReader = function(deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readAsDataURL = function (file, scope) {
        var deferred = $q.defer();
         
        var reader = getReader(deferred, scope);         
        reader.readAsDataURL(file);
         
        return deferred.promise;
    };

    return {
            readAsDataUrl: readAsDataURL  
    };
}]);


appActivities.controller('MainCtrl', function (
    $scope,
    $routeParams,
    $timeout,
    $rootScope,
    dataService,
    ModelUtils,
    $dialogs,
    fileReader
    ) {

    $scope.getFile = function() {
        if(!$scope.course){
            $scope.course = {};
        }
        fileReader.readAsDataUrl($scope.file, $scope)
                      .then(function(result) {
                          $scope.course.image = result;
                      });
    };

    var delay = 1;
    var delaySuccessClass = function(objSaved){
        objSaved.success=true;
        $timeout(function(){
            if(delay < 2){
                delay += 1;
                delaySuccessClass(objSaved);
            }else{
                $scope.$apply(function() {
                    objSaved.success=false;
                });                    
            }
        },500);// 500ms
    }; // end delaySuccessClass 

    
    //dlg = $dialogs.create('/dialogs/whatsyourname.html','whatsYourNameCtrl',{},{key: false,back: 'static'});
    var isEmpty = function (obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    }


    var saveObj = function(obj, objs, url){
        obj.errors = {};
        return ModelUtils.save(url, obj, obj.errors).then(function(response){
            // Push object into objects only if is not exist.
            if(objs) {
                if(objs.indexOf(obj) == -1){
                    objs.push(obj);
                }
            }
            //dlg = $dialogs.wait(msgs[i++],progress);
            //fakeProgress();

        });
    };

    var delObj = function(obj, objs, url){
        ModelUtils.del(url, obj).then(function(response){
            var index =  objs.indexOf(obj);
            if (index > -1) {
                objs.splice(index, 1);
            }
        });
    };


    /* 
        dayNames
    */
    var URL_DAY_NAMES = '/activities/api/day_names/';
    var getDayNames = function(){
        ModelUtils.get(URL_DAY_NAMES, null).then(function(response){
            $scope.dayNames = response.data;
        });
    };


    /* 
        Teacher
    */
    var URL_TEACHERS = '/activities/api/teachers/';
    $scope.newTeacher = function(){
        clearTeacher();
        $scope.isModTeacher = true;
    };

    $scope.modTeacher = function(){
        $scope.isModTeacher = !$scope.isModTeacher;
    };

    $scope.saveTeacher = function(){
        var isSaved = saveObj($scope.teacher, $scope.teachers, URL_TEACHERS);
        isSaved.then(function (res){
            delaySuccessClass($scope.teacher);
            $scope.isModTeacher = false;
        });
    };

    $scope.delCancelTeacher = function(){
        // The function is also called when the obj is not saved
        // so call the rest only if the object has an id.
        if( $scope.teacher != null && $scope.teacher.id != null) {
            delObj($scope.teacher, $scope.teachers, URL_TEACHERS);
        }
        clearTeacher();
        $scope.isModTeacher = false;
    };

    $scope.selectTeacher = function(teacherOpt){
        $scope.teacher = teacherOpt;
    };

    var clearTeacher = function(){
        $scope.teacher = null;
    };

    var getTeachers = function (){
        ModelUtils.get(URL_TEACHERS, null).then(function(response){
            $scope.teachers = response.data;
        });
    };

    $scope.$watch('teacher', function(newValue, oldValue) {
        getCourse();
    });

    /*
    $scope.$watch('teachers.length', function(newValue, oldValue) {
        //getTeachers()
    });
    */

    /* 
        CourseName
    */
    var URL_COURSE_NAMES = '/activities/api/course_names/';
    $scope.newCourseName = function(){
        $scope.courseName = null;
        $scope.isModCourseName = true;
    };

    $scope.modCourseName = function(){
        $scope.isModCourseName = !$scope.isModCourseName;
    };

    $scope.saveCourseName = function(){
        var isSaved = saveObj($scope.courseName, $scope.courseNames, URL_COURSE_NAMES);
        isSaved.then(function (res){
            delaySuccessClass($scope.courseName);
            $scope.isModCourseName = false;
        });
    };

    $scope.delCancelCourseName = function(){
        if( $scope.courseName != null && $scope.courseName.id != null) {
            delObj($scope.courseName, $scope.courseNames, URL_COURSE_NAMES);
        }
        clearCourseName();
        $scope.isModCourseName = false;
    };

    $scope.selectCourseName = function(courseNameOpt){
        $scope.courseName = courseNameOpt;
    };

    var clearCourseName = function(){
        $scope.courseName = null;
    };

    var getCourseNames = function (){
        clearCourseName();
        ModelUtils.get(URL_COURSE_NAMES, null).then(function(response){
            $scope.courseNames = response.data;
        });
    };

    $scope.$watch('courseName', function(newValue, oldValue) {
        getCourse();
    });

    /*
        Course
    */
    var URL_COURSES = '/activities/api/courses/';
    $scope.newCourse = function(){
        $scope.course = null;
        $scope.isModCourse = true;
    };

    $scope.modCourse = function(){
        $scope.isModCourse = !$scope.isModCourse;
    };

    $scope.saveCourse = function(){
        $scope.course.teacher = $scope.teacher.id;
        $scope.course.course_name = $scope.courseName.id;

        var isSaved = saveObj($scope.course, [], URL_COURSES);
        isSaved.then(function (res){
            delaySuccessClass($scope.course);
            $scope.isModCourse = false;
        });
    };

    $scope.delCancelCourse = function(){
        if( $scope.course != null && $scope.course.id != null) {
            delObj($scope.course, [], URL_COURSES);
        }
        clearCourse();
        $scope.isModCourse = false;
    };

    var clearCourse = function(){
        $scope.course = null;
        $scope.isModCourse = false;
    };


    var getCourse = function (){
        clearCourse();
        if( $scope.teacher != null && $scope.teacher.id &&
            $scope.courseName != null && $scope.courseName.id){
            var url = URL_COURSES + '?teacher=' + $scope.teacher.id + 
                        '&course_name=' + $scope.courseName.id;
            ModelUtils.get(url, null).then(function(response){
                $scope.course = response.data[0];
            });
        }
    };

    $scope.$watch('course', function(newValue, oldValue) {
        getSchedules();
    });

    /* 
        schedule
    */
    var URL_SCHEDULE = '/activities/api/schedules/';
    $scope.newSchedule = function(){
        $scope.schedule = {};
        $scope.isModSchedule = true;
    };

    $scope.modSchedule = function(){
        $scope.isModSchedule = !$scope.isModSchedule;
    };

    $scope.saveSchedule = function(){
        // Need to assign the current corse id to the selected schedule
        $scope.schedule.course = $scope.course.id;

        var isSaved = saveObj($scope.schedule, $scope.schedules, URL_SCHEDULE);
        isSaved.then(function (res){
            delaySuccessClass($scope.schedule);
            $scope.isModSchedule = false;
        });
    };

    $scope.delCancelSchedule = function(){
        if( $scope.schedule != null && $scope.schedule.id != null) {
            delObj($scope.schedule, $scope.schedules, URL_SCHEDULE);
        }
        $scope.schedule = null;
        $scope.isModSchedule = false;
    };

    $scope.selectSchedule = function(scheduleOpt){
        $scope.schedule = scheduleOpt;
    };

    var clearSchedule = function(){
        $scope.schedule = null;
        //$scope.schedules = null;
    };


    var getSchedules = function (){
        clearSchedule();
        if($scope.course != null && $scope.course.id ){
            var url = URL_SCHEDULE + '?course=' + $scope.course.id;
            ModelUtils.get(url, null).then(function(response){
                $scope.schedules = response.data;
            });
        }
    };

    $scope.$watch('schedule', function(newValue, oldValue) {
        getDaySchedules();
    });


    /* 
        daySchedule 
    */
    var URL_DAY_SCHEDULES = '/activities/api/day_schedules/';

    var getDayNameName = function(day_name_id){
        if($scope.dayNames != null) {
            for (var i = 0; i < $scope.dayNames.length; i++){
                if(day_name_id == $scope.dayNames[i].id) {
                    return $scope.dayNames[i].name;
                }
            }
        }
        return "";
    };

    $scope.saveDaySchedule = function(daySchedule){
        daySchedule.schedule = $scope.schedule.id;
        
        var isSaved = saveObj(daySchedule, $scope.daySchedules, URL_DAY_SCHEDULES);
        isSaved.then(function (res){
            delaySuccessClass(daySchedule);
        });
    };

    $scope.saveDayScheduleNew = function(){
        $scope.dayScheduleNew.schedule = $scope.schedule.id;
        if(!$scope.daySchedules){
            $scope.daySchedules = [];
        }

        var isSaved = saveObj($scope.dayScheduleNew, $scope.daySchedules, URL_DAY_SCHEDULES);
        isSaved.then(function (res){
            delaySuccessClass($scope.dayScheduleNew);
        });

        $scope.dayScheduleNew = {};
    };




    $scope.delCancelDaySchedule = function(daySchedule){
        console.log('delCancelSchedule');
        if(daySchedule != null && daySchedule.id ){
            delObj(daySchedule, $scope.daySchedules, URL_DAY_SCHEDULES);
        }else{
            daySchedule.hour_start = "";
            daySchedule.hour_end = "";
            daySchedule.day_name = null;
            daySchedule.day_name_name = null;
        }
    };



    $scope.selectDayName = function(dayName, daySchedule){
        console.log(daySchedule);
        daySchedule.day_name = dayName.id;
        daySchedule.day_name_name = getDayNameName(dayName.id);
    };



    var clearDaySchedule = function(){
        $scope.daySchedule = null;
        $scope.daySchedules = null;
    };

    var getDaySchedules = function (){
        clearDaySchedule();
        if($scope.schedule != null && $scope.schedule.id){
            var url = URL_DAY_SCHEDULES + '?schedule=' + $scope.schedule.id;
            ModelUtils.get(url, null).then(function(response){
                $scope.daySchedules = response.data;
                for (var i = 0; i < $scope.daySchedules.length; i++){
                    $scope.daySchedules[i].day_name_name = 
                            getDayNameName($scope.daySchedules[i].day_name);
                }
            });
        }
    };

    /*
    $scope.$watch('daySchedules.length', function(newValue, oldValue) {
        getDaySchedules();
    });
    */

    $scope.dayScheduleNew = {};
    $scope.daySchedules = {};

    getCourseNames();
    getTeachers();
    getDayNames();


}); // end run / module

