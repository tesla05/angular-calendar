appActivities.factory('TeacherFact', function( $log ) {

    // PageBreakBlock Class
    function PageBreakBlock ( order ) {
        this.order = order;
        this.code = PAGE_BREAK_BLOCK_CODE;
    }

    var PageBreakBlockFact = {

        // Constructor
        PageBreakBlock: function( order ) {
            return new PageBreakBlock( order );
        },
    };
    return PageBreakBlockFact;
});