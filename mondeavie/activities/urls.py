from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from rest_framework.urlpatterns import format_suffix_patterns
import views

urlpatterns = patterns('',
    url(r'^api/teachers/$', views.TeacherList.as_view()),
    url(r'^api/teachers/(?P<pk>[0-9]+)$', views.TeacherDetail.as_view()),

    url(r'^api/course_names/$', views.CourseNameList.as_view()),
    url(r'^api/course_names/(?P<pk>[0-9]+)$', views.CourseNameDetail.as_view()),


    #url(r'^api/course_categories/$', views.CourseCategoryList.as_view()),
    #url(r'^api/course_categories/(?P<pk>[0-9]+)$', views.CourseCategoryDetail.as_view()),


    url(r'^api/courses/$', views.CourseList.as_view()),
    url(r'^api/courses/(?P<pk>[0-9]+)$', views.CourseDetail.as_view()),


    url(r'^api/schedules/$', views.ScheduleList.as_view()),
    url(r'^api/schedules/(?P<pk>[0-9]+)$', views.ScheduleDetail.as_view()),


    url(r'^api/day_names/$', views.DayNameList.as_view()),
    url(r'^api/day_names/(?P<pk>[0-9]+)$', views.DayNameDetail.as_view()),

    url(r'^api/day_schedules/$', views.DayScheduleList.as_view()),
    url(r'^api/day_schedules/(?P<pk>[0-9]+)$', views.DayScheduleDetail.as_view()),


    url(r'^login/$', views.LoginView.as_view(), name='accounts-login'),
)

#urlpatterns = format_suffix_patterns(urlpatterns)