# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG
SEND_BROKEN_LINK_EMAILS = not DEBUG


STATIC_ROOT = os.path.join(BASE_DIR, 'static', 'static_root')
MEDIA_ROOT = os.path.join(BASE_DIR, 'static', 'media')

STATIC_URL = '/static/'
MEDIA_URL = '/media/'


IS_AUTO_LOGIN = True
IS_MANIFEST = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}